import com.zuitt.example.*;

public class Main {
    public static void main(String[] args) {
//        [OOP - Object-Oriented Programming]
//         - a programming model that allows developers to design software around data or objects, rather than function and logic.

//        OOP Concept
//          - OBJECT
//              > abstract idea that represents something in the real world.
//        Example: The concept of a dog.

//          - CLASS
//              > representation of object using code. (Blueprint/framework)
//        Example: Writing a code that would describe a dog.

//          - Instance
//              > unique copy of the idea, made physical.
//        Example: Instantiating a dog named Brownie from the dog class.

//         Objects
//              - States and attributes
//                  > What is the idea about? It would describe the properties that the object has.
//              - Behaviors - What can idea do?
//        Example: A person has attributes like name, age, height, and weight. A person can also eat, sleep, speak.

//Four Pillars of OOP
        //1. Encapsulation
            //a mechanism of wrapping the data (variables) and code acting on the data (methods) together as a single unit.
            //"data handling" - the variables of a class will be hidden from other classes, and can be accessed only through the methods of the current class
            //variables/properties as private
            //provide a public setter and getter function.

//        ======================================================
//        Instantiate a "class Car" with empty argument.
        Car myCar = new Car("Vios", "Toyota", 2025 );

//       Getters
        System.out.println(myCar.getName());
        System.out.println(myCar.getBrand());
        System.out.println(myCar.getYearOfMake());

//       Setters
        myCar.setName("Paeng");
        System.out.println(myCar.getName());

        Car emptyCar = new Car();
        System.out.println(emptyCar.getName());


//        Composition and Inheritance
//          - both concepts promotes code reuse through different

//        Inheritance - allows modelling an object that is a subset of another object.

//        Composition - allows modelling object that are made of other objects.
        System.out.println(myCar.getDriverName());
        myCar.setDriverName("Kuya");
        System.out.println(myCar.getDriverName());

        Dog myPet = new Dog("Browny", "brown", "aspi");

        System.out.println(myPet.getName());
        myPet.setName("Black");
        System.out.println(myPet.getName());
        myPet.call();
        System.out.println(myPet.getBreed());
        myPet.makeSound();

//        Abstraction
//          - a process where all the logic and complexity are hidden from the user.

//        Interface
//          - used to achieve total abstraction
//          - creating abstract classes doesn't support multiple inheritance.

        Person child = new Person();

        child.sleep();
        child.run();
        child.morningGreet();
        child.afternoonGreet();

//        Polymorphism
//          - Greek word "poly"(many) and "morph"(forms);
//          - many forms.

//        Two main Types
//        1. Static/Compile time polymorphism - overloading( more than one version of method)
//        2. Dynamic/run-time polymorphism - overridden the method.

        StaticPoly myAddition = new StaticPoly();

        myAddition.addition(5,5);
        myAddition.addition(5,5,5);
        myAddition.addition(5.5,5.5);

        Parent myParent = new Parent();
        Child myChild = new Child();

        myParent.speak();
        myChild.speak();
    }
}