package com.zuitt.example;

//Chill class of Animal class
// "extends" keyword is used to inherit all the properties and method of the parent class.
public class Dog extends Animal {
    private String breed;
    public  Dog() {
        super();
    }
    public Dog(String name, String color, String breed) {
      super(name, color);
      this.breed = breed;
    }
//    Getter
    public String getBreed() {
        return this.breed;
    }
//    Setter
    public void setBreed(String breed) {
        this.breed = breed;

    }

    public void makeSound() {
        System.out.println("Woof! woof!");
    }
}
