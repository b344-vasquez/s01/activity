import java.util.ArrayList;


public class Main {
    public static void main(String[] args) {

        Phonebook phonebook = new Phonebook();

        ArrayList<Contact> newContacts = new ArrayList<>();


        Contact contact1 = new Contact("John Doe", "123-456-7890","123-456-7890","555 Main St","555 Main St");
        Contact contact2 = new Contact("Jane Doe", "987-654-3210","123-456-7890","456 Elm St","555 Main St");

        newContacts.add(contact1);
        newContacts.add(contact2);

        phonebook.setContacts(newContacts);

        ArrayList<Contact> retrievedContacts = phonebook.getContacts();


        if(retrievedContacts.isEmpty()){
            System.out.println("Phonebook is empty");
        } else {
            for (Contact contact : retrievedContacts) {

                System.out.println(contact.toString());
                System.out.println();
            }
        }
    }
}
