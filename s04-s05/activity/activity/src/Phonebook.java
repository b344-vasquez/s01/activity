import java.util.ArrayList;


class Phonebook {
        private ArrayList<Contact> contacts;

        // Default constructor
        public Phonebook() {
                contacts = new ArrayList<>();
        }

        // Parameterized constructor
        public Phonebook(ArrayList<Contact> contacts) {
                this.contacts = contacts;
        }

        // Getter and Setter methods
        public ArrayList<Contact> getContacts() {
                return (ArrayList<Contact>) contacts;
        }

        public void setContacts(ArrayList<Contact> contacts) {
                this.contacts = contacts;
        }

}
