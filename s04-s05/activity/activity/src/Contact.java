class Contact {
    private String name;
    private String contactNumber;
    private String address;

    // Default constructor
    public Contact() {
    }

    // Parameterized constructor
    public Contact(String name, String contactNumber, String address) {
        this.name = name;
        this.contactNumber = contactNumber;
        this.address = address;
    }
    public Contact(String name, String contactNumber1,String contactNumber2, String address1, String address2) {
        this.name = name;
        this.contactNumber = contactNumber1 + "\n" + contactNumber2;
        this.address = address1 + "\n"+ address2;
    }

    // Getter and Setter methods for Name
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    // Getter and Setter methods for ContactNumber
    public String getContactNumber() {
        return contactNumber;
    }

    public void setContactNumber(String contactNumber) {
        this.contactNumber = contactNumber;
    }


    // Getter and Setter methods for Address
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String toString() {
        return  name + "\n" + "----------" +"\n"+
                name + " has the following registered number/s" +"\n"+
                contactNumber + "\n" +
                name + " has the following registered address/es" +"\n"+
                address;
    }
}
