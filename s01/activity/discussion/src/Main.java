import java.util.Scanner;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
//    public - access modifier which simply tells the application which classes have access to this method/attribute.
//    static - keyword associated with method/property that is related in class. This will allow a method to be invoked without instantiating a class.
//    void - keyword that is used to specify a method that doesn't return any value. In Java, we have to declare the data type of the method's return.
//    String[] args - accepts a single argument of type String array that contains command line argument.

    public static void main(String[] args) {
        Scanner myObj = new Scanner(System.in);

        System.out.println("First Name: ");
        String fName = myObj.nextLine();

        System.out.println("Last Name: ");
        String lName = myObj.nextLine();

        System.out.println("First Subject Grade: ");
        double fGrade = myObj.nextDouble();

        System.out.println("Second Subject Grade: ");
        double sGrade = myObj.nextDouble();

        System.out.println("Third Subject Grade: ");
        double tGrade = myObj.nextDouble();

        double ave = (fGrade + sGrade + tGrade) / 3;

        System.out.println("Good day, " + fName + " " + lName + ".");
        System.out.println("Your grade average is: " + ave);


        }
    }
