import java.util.Scanner;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int num = 1;

        int factWhile = 1; // answer variable for while-loop
        int factFor = 1; // answer variable for for-loop


        System.out.println("Input an integer whose factorial will be computed: ");
        try
        {
            num = in.nextInt();
        } catch (Exception e)
        {
            System.out.println("Invalid input!");
            e.printStackTrace();
        }


//      [WHILE-LOOP]
        int countWhile = 1;

        while (countWhile <= num)
        {
            factWhile *= countWhile;
            countWhile++;
        }

//        System.out.println("Answer Using While-Loop:");
        System.out.println("The factorial of " + num + " is " + factWhile);

//      [FOR-LOOP]
        for(int countFor = 1; countFor <= num; countFor++)
        {
            factFor *= countFor;
        }
        System.out.println();
        System.out.println("Answer Using For-Loop:");
        System.out.println("The factorial of " + num + " is " + factFor);
    }
}