import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

// Press Shift twice to open the Search Everywhere dialog and type `show whitespaces`,
// then press Enter. You can now see whitespace characters in your code.
public class Main {
    public static void main(String[] args) {
//        [ACTIVITY] Array
        int[] intArray = new int[5];
        intArray[0] =2;
        intArray[1] =3;
        intArray[2] =5;
        intArray[3] =7;
        intArray[4] =11;

        System.out.println("[ACTIVITY] Array");

        System.out.println(Arrays.toString(intArray));
        System.out.println("The first prime number is: " + intArray[0]);

        System.out.println("The second prime number is: " + intArray[1]);

        System.out.println("The third prime number is: " + intArray[2]);

        System.out.println("The forth prime number is: " + intArray[3]);

        System.out.println("The fifth prime number is: " + intArray[4]);

//        [ACTIVITY] arrayList
        ArrayList<String> friends = new ArrayList<String>(Arrays.asList("John","Jane","Chole","Zoey"));

        System.out.println("[ACTIVITY] arrayList");
        System.out.println("My friends are: " + friends);

//        [ACTIVITY] HashMap
        HashMap<String, Integer> inventory = new HashMap<String, Integer>(){
            {
                put("toothpaste", 15);
                put("toothbrush", 20);
                put("soap", 12);
            }
        };
        System.out.println("[ACTIVITY] HashMap");
        System.out.println("Our current inventory consist of: " +inventory);



    }
}