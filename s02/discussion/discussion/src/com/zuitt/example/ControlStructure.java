package com.zuitt.example;

import java.util.Scanner;

public class ControlStructure {
    public static void main(String[] args) {
//        [SECTION] Java Operators
//        Arithmetic: +, -, *, /, %;
//        Comparison: <, >, <=, >=, ==, !=;
//        Logical: &&, ||, !;
//        Re/assignment: =, +=, -=, *=, /=;
//        Increment and Decrement: i++, ++i, --i, i--;

//        [SECTION] Selection Control Structure;
//        - statement allows us to manipulate the flow of the code depending on the evaluation of the condition;
        /*
            if(condition) {
                (statement)
            }
            else if {
                (statement)
            }
            else {
                (statement)
            }
         */
        int num1 = 36;
        if(num1 % 5 == 0) {
            System.out.println(num1 + " is divisible by 5.");
        } else {
            System.out.println(num1 + " is not divisible by 5.");
        }

//        [SECTION] Short Circuiting
//          - a technique applicable ONLY to the AND & OR operators wherein "if statement/s" or other control exist early by ensuring safety operation or for efficiency.
//        if(condition1 || condition2 || condition3 || condition4)

//        [SECTION] Ternary Operator
        int num2 = 24;
        Boolean result = (num2 > 0) ? true: false;
        System.out.println(result);

//        [SECTION] Switch Statement
//          - use to control the flow structures that allow one code block out of many other code blocks;

        Scanner numberScanner = new Scanner(System.in);
        System.out.println("Enter a number: ");

        int directionValue = numberScanner.nextInt();

        switch (directionValue) {
            case 1:
                System.out.println("North");
                break;
            case 2:
                System.out.println("South");
                break;
            case 3:
                System.out.println("East");
                break;
            case 4:
                System.out.println("West");
                break;
            default:
                System.out.println("Invalid");
        }
    }
}
